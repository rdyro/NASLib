import pickle
from pathlib import Path
from typing import Any, Dict, Tuple, Union

import torch
import torch.nn.functional as F
import zstandard as zstd
from torch import Tensor
from torch.utils.data import DataLoader

from ...utils.utils import get_project_root


def _get_data() -> Dict[str, Union[Any, Tuple[Tensor, Tensor]]]:
    path = (Path(get_project_root()) / "data" / "timit_dataloaders.pkl.zst").absolute()
    assert path.exists()
    with zstd.open(path, "rb") as fp:
        data = pickle.load(fp)
    return data  # dict(encoder=..., train=(X_train, y_train), test=(X_test, y_test))


def get_random_batch(
    batch_size: int = 64,
    train_or_test: str = "train",
    dataset_name: str = "timit",
) -> Tuple[Tuple[Tensor, Tensor], Tuple[Tensor, Tensor]]:
    assert dataset_name in ["timit"]
    self = get_random_batch
    if not hasattr(self, "data"):
        self.data = _get_data()
        self.X_train, self.y_train = self.data["train"]
        self.X_test, self.y_test = self.data["test"]
    if train_or_test == "train":
        ridx = torch.randperm(self.X_train[0].shape[0])[:batch_size]
        X, y = self.X_train, self.y_train
    elif train_or_test == "test":
        ridx = torch.randperm(self.X_test[0].shape[0])[:batch_size]
        X, y = self.X_test, self.y_test
    else:
        raise ValueError(
            f"`train_or_test` argument = {train_or_test} is unsupported. Use 'train' or 'test'"
        )
    return ((X[0][ridx, ...], X[1][ridx, ...]), (y[0][ridx, ...], y[1][ridx, ...]))


def loss_fn(model, audio_input, target_input):
    audio, audio_len = audio_input
    target, target_len = target_input

    output = model(audio)
    output = F.log_softmax(output, dim=2)
    output_len = audio_len // 4

    output_trans = output.permute(1, 0, 2)  # needed by the CTCLoss
    loss = F.ctc_loss(
        output_trans, target, output_len, target_len, reduction="none", zero_infinity=True
    )
    loss /= output_len
    loss = loss.mean()
    return loss

from pathlib import Path
from typing import Any, Dict, Tuple
from collections import namedtuple

import torch
from torch import nn
import torch.nn.functional as F
from torch import Tensor

from nas_bench_nlp.data import Corpus
from nas_bench_nlp.utils import batchify
from nas_bench_nlp.splitcross import SplitCrossEntropyLoss

from ...utils.utils import get_project_root

def get_dataset(dataset_name: str = "ptb") -> Dict[str, Any]:
    path = (Path(get_project_root()) / "data" / "nlp_data" / dataset_name).absolute()
    assert path.exists()
    return Corpus(path)


def get_random_batch(
    batch_size: int = 64, train_or_test: str = "train", dataset_name: str = "ptb"
) -> Tuple[Tensor, Tensor]:
    assert dataset_name in ["ptb", "wikitext-2"]
    self = get_random_batch
    if not hasattr(self, "data"):
        self.data = dict()
    if dataset_name not in self.data:
        corpus = get_dataset(dataset_name)
        self.data[dataset_name] = dict(dataset=corpus)
        train = batchify(corpus.train, 50, namedtuple("args", "cuda")(False))
        test = batchify(corpus.test, 50, namedtuple("args", "cuda")(False))
        self.data[dataset_name]["train"] = train
        self.data[dataset_name]["test"] = test
    assert train_or_test in ["train", "test"]
    X = self.data[dataset_name][train_or_test]
    X = X[torch.randperm(X.shape[0]), ...]
    input, target = X[:batch_size, ...], X[1 : batch_size + 1, ...]
    return input, target


def loss_fn(model: nn.Module, input: Tensor, target: Tensor) -> Tensor:
    criterion = SplitCrossEntropyLoss(400, splits=[], verbose=False)
    hidden = model.init_hidden(input.shape[-1])
    output, hidden = model(input, hidden)
    loss = criterion(model.net.decoder.weight, model.net.decoder.bias, output, target)
    return loss

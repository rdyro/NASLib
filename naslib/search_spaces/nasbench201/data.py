import pickle
from collections import OrderedDict
from pathlib import Path
from typing import Any, Dict, Tuple

import torch
from torch import Tensor, nn
from torch.utils.data import DataLoader
from xautodl.datasets import get_dataset_with_transform
from xautodl.models import get_cell_based_tiny_net

from ...utils.utils import get_project_root


def _get_dataset(dataset_name: str):
    assert dataset_name in ["cifar10", "cifar100", "ImageNet16-120"]
    path = Path(get_project_root()) / "data"
    if dataset_name == "ImageNet16-120":
        path = path / "ImageNet16"
    return get_dataset_with_transform.get_datasets(dataset_name, path, -1)


def get_random_batch(
    batch_size: int = 64,
    train_or_test: str = "train",
    dataset_name: str = "cifar10",
) -> Tuple[Tuple[Tensor, Tensor], Tuple[Tensor, Tensor]]:
    self = get_random_batch
    if not hasattr(self, "datasets"):
        self.datasets = dict()
    if dataset_name not in self.datasets:
        self.datasets[dataset_name] = OrderedDict(
            zip(("train", "test"), _get_dataset(dataset_name)[:2])
        )
    assert train_or_test in ["train", "test"]
    dl = DataLoader(self.datasets[dataset_name][train_or_test], batch_size=batch_size, shuffle=True)
    return next(iter(dl))


def loss_fn(model, input, target):
    return torch.nn.CrossEntropyLoss()(model(input), target)


def build_net_from_archstr(archstr: str, dataset: str) -> nn.Module:
    self = build_net_from_archstr
    if not hasattr(self, "archstr2config"):
        with open(
            (Path(get_project_root()) / "data" / "nasbench201_archstr2config.pkl").absolute(), "rb"
        ) as fp:
            self.archstr2config = pickle.load(fp)
    return get_cell_based_tiny_net(self.archstr2config[archstr][dataset])

from pathlib import Path
from typing import Any, Dict, Tuple

import torch
from torch import Tensor
from torch.utils.data import DataLoader
from collections import OrderedDict

from xautodl.datasets import get_dataset_with_transform

from ...utils.utils import get_project_root


def _get_dataset():
    path = Path(get_project_root()) / "data"
    return get_dataset_with_transform.get_datasets("cifar10", path, -1)


def get_random_batch(
    batch_size: int = 64,
    train_or_test: str = "train",
    dataset_name: str = "cifar10",
) -> Tuple[Tuple[Tensor, Tensor], Tuple[Tensor, Tensor]]:
    assert dataset_name in ["cifar10"]
    self = get_random_batch
    if not hasattr(self, "dataset"):
        self.dataset = OrderedDict(zip(("train", "test"), _get_dataset()[:2]))
    assert train_or_test in ["train", "test"]
    dl = DataLoader(self.dataset[train_or_test], batch_size=batch_size, shuffle=True)
    return next(iter(dl))


def loss_fn(model, input, target):
    return torch.nn.CrossEntropyLoss()(model(input), target)
